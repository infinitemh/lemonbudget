# pull official base image
FROM python:3.8.0-alpine

# set work directory
WORKDIR /usr/src/lemon_budget

# set environment variables
# Prevents Python from writing pyc files to disc (equivalent to python -B option)
ENV PYTHONDONTWRITEBYTECODE 1
# Prevents Python from buffering stdout and stderr (equivalent to python -u option)
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

# install dpendencies
RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/lemon_budget/requirements.txt
RUN pip install -r requirements.txt

# copy entrypoint.sh
COPY ./entrypoint.sh /usr/src/lemon_budget/entrypoint.sh

# copy project
COPY . /usr/src/lemon_budget

# run entrypoint.sh
ENTRYPOINT ["/usr/src/lemon_budget/entrypoint.sh"]